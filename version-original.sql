CREATE SCHEMA dbo;

CREATE TABLE Productos ( 
	codProductos         int NOT NULL   ,
	nombre               varchar(50)    ,
	precio               money    ,
	CONSTRAINT Pk_Productos_codProductos PRIMARY KEY  ( codProductos )
 );

CREATE TABLE clientes ( 
	idClientes           int NOT NULL   ,
	nombre               varchar(50)    ,
	apellido             varchar(50)    ,
	edad                 int    ,
	CONSTRAINT Pk_clientes_idClientes PRIMARY KEY  ( idClientes )
 );

CREATE TABLE ventas ( 
	idVentas             int NOT NULL   ,
	idProductos          int    ,
	idClientes           int    ,
	fecha                date    ,
	CONSTRAINT Pk_ventas_idVentas PRIMARY KEY  ( idVentas )
 );

CREATE  INDEX Idx_ventas_idClientes ON ventas ( idClientes );

CREATE  INDEX Idx_ventas_idProductos ON ventas ( idProductos );

ALTER TABLE ventas ADD CONSTRAINT fk_ventas_productos FOREIGN KEY ( idProductos ) REFERENCES Productos( codProductos ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE ventas ADD CONSTRAINT fk_ventas_clientes FOREIGN KEY ( idClientes ) REFERENCES clientes( idClientes ) ON DELETE CASCADE ON UPDATE CASCADE;

